import java.io.Serializable;

// This is the object that the "reader" program will read
class SampleObject implements Serializable
{
    // The values stored doesn't matter for the demonstration of the exploit
    // They are just set to a string value and an int that the reader will show
    public String name;
    public int value;

    // Default constructor
    public SampleObject(String name, int value)
    {
        this.name = name;
        this.value = value;
    }
}
