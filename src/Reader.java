import java.io.*;

// This class deserialized objects and outputs their values
class Reader
{
    public static void main(String[] args)
    {
        //Attempt to deserialize the object
        Object received = deserialize();
        SampleObject receievedFormatted = (SampleObject)received;
        System.out.println(receievedFormatted.name);
        System.out.println(receievedFormatted.value); 
    }

    // The function to deserialize the file "object.ser"
    public static Object deserialize()
    {
        Object receivedObject = null;
        try
        {
            FileInputStream file = new FileInputStream("object.ser");
            ObjectInputStream in = new ObjectInputStream(file);

            receivedObject = in.readObject();

            in.close();
            file.close();

            System.out.println("Object has been deserialized");   
        }
        catch(IOException exception)
        {
            System.out.println("IOException error: " + exception.toString());
        }
        catch(ClassNotFoundException exception)
        {
            System.out.println("Class not found exception");
        }
        return receivedObject;
    }
}
