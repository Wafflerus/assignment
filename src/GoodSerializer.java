import java.io.*;

// This class is an example of normal use. It creates and serializes an object
// which is for normal use and is not malicious
class GoodSerializer
{
    public static void main(String[] args)
    {
        SampleObject goodObject = new SampleObject("caleb", 1024);
        serialize(goodObject);
    }
    
    // The function to serialize the data
    public static void serialize(Object object)
    {
        try
        {
            FileOutputStream file = new FileOutputStream("object.ser");
            ObjectOutputStream out = new ObjectOutputStream(file);

            out.writeObject(object);

            out.close();
            file.close();

            System.out.println("Object has been serialized");
        }
        catch(IOException exception)
        {
            System.out.println("IOException error: " + exception.toString());
        }
    }


}
