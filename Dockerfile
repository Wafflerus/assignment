FROM openjdk:11

#Set environment variable with location of working directory
ENV PWD=/usr/testproj/

#Copy contents of pwd to environment variable
COPY ./ $PWD
WORKDIR $PWD

#should be source
RUN javac ./src/*.java \
 && export CLASSPATH=$PWD/patched/jars/cc4/commons-collections4-4.0.jar":"$PWD 

